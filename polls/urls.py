from django.conf.urls import url
from django.views.generic import TemplateView

from . import views

urlpatterns = [
    # ex: /polls/
    url(r'^$', views.index, name='strategy'),  
    url(r'^best_strategy/$', views.best_strategy, name='best_strategy'),
    url(r'^best_opt_strategy/$', views.best_opt_strategy, name='best_opt_strategy'),
    url(r'^top_stocks/$', views.top_stocks, name='top_stocks'),
    url(r'^stocks_profit_of_strategies/(?P<strategy>.+)/$', views.stocks_profit_of_strategies, name='stocks_profit_of_strategies'),
    url(r'^strategies_profit_of_stocks/(?P<stock>.+)/$', views.strategies_profit_of_stocks, name='strategies_profit_of_stocks')
]
