from django.http import HttpResponse
from django.template import loader
from django.shortcuts import get_object_or_404, render
from .models import StocksStrategy
from django.db.models import Max
import exteme


def index(request):
    dictt = StocksStrategy.objects.filter(default_value = True).distinct('strategies')
    return render(request, 'polls/strategy.html', {'dictt':(dictt,{},'Please Select Strategy')})

def best_strategy(request):
    dictt = StocksStrategy.objects.filter(default_value = True).distinct('scrip')
    return render(request, 'polls/best_strategy.html',{'dictt':(dictt, {}, 'Please Select Stock')})

def best_opt_strategy(request):
    dictt = StocksStrategy.objects.raw('select * from (select distinct on (s.scrip) s.scrip, s.id, s.profit from polls_stocksstrategy s where s.default_value = False and profit in (select max(profit) from polls_stocksstrategy where default_value = False group by scrip)) sb order by sb.profit DESC')
    return render(request, 'polls/best_opt_strategy.html',{'dictt':dictt})

def top_stocks(request):
    dictt = StocksStrategy.objects.raw('select *, profit as max_profit from polls_stocksstrategy where profit in (select max(profit) from polls_stocksstrategy where default_value = True group by scrip) and default_value= True order by profit DESC')
    return render(request, 'polls/top_stocks.html',{'dictt':dictt})

def stocks_profit_of_strategies(request, strategy):
    dictt = StocksStrategy.objects.filter(default_value = True).distinct('strategies')
    scripts_profit = StocksStrategy.objects.filter(strategies=strategy, default_value = True).order_by('-profit')
    return render(request, 'polls/strategy.html',{'dictt' : (dictt, scripts_profit,strategy)})

def strategies_profit_of_stocks(request, stock):
    dictt = StocksStrategy.objects.filter(default_value = True).distinct('scrip')
    scripts_profit = StocksStrategy.objects.filter(scrip=stock, default_value = True).order_by('-profit')
    return render(request, 'polls/best_strategy.html',{'dictt' : (dictt, scripts_profit, stock)})