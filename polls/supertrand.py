import pandas as pd

def supertrand(data, period = 10, multiplier = 3):
    h_l = data.H - data.L
    h_cp = abs(data.H - data.C.shift())
    l_cp = abs(data.L -data.C.shift())
    data1 = pd.DataFrame()
    data1['h_l'] = h_l
    data1['h_cp'] = h_cp
    data1['l_cp'] = l_cp
    tr = data1[["h_l", "h_cp", "l_cp"]].max(axis=1)
    atr = pd.rolling_mean(tr, period)
    upperband_basic = ((data.H + data.L)/2) + (multiplier * atr)
    lowerband_basic = ((data.H + data.L) / 2) - (multiplier * atr)
    upperband = upperband_find(upperband_basic, data.C, period)
    lowerband = lowerband_find(lowerband_basic, data.C, period)
    supertrnd = supertrand_find(upperband, lowerband, data.C,period)
    return supertrnd

def upperband_find(upband_basic, close, period):
    uprband = upband_basic * 1
    for index in range(len(upband_basic)):
        if (index > period - 1):
            if(upband_basic[index] < uprband[index - 1] or close[index - 1] > uprband[index - 1]):
                uprband[index] = upband_basic[index]
            else:
                uprband[index] = uprband[index - 1]
    return uprband

def lowerband_find(lowerband_basis, close, period):
    lwrband = lowerband_basis * 1
    for index in range(len(lowerband_basis)):
        if(index > period - 1):
            if (lowerband_basis[index] > lwrband[index - 1] or close[index - 1] < lwrband[index - 1]):
                lwrband[index] = lowerband_basis[index]
            else:
                lwrband[index] = lwrband[index - 1]
    return lwrband

def supertrand_find(uperband, lowerband, close, period):
    supertrand = uperband * 1
    for index in range(len(uperband)):
        if (index > period - 1):
            if ((supertrand[index -1] == uperband[index -1]) & (close[index] < uperband[index])):
                supertrand[index] = uperband[index]
            else:
                if((supertrand[index -1] == uperband[index -1]) & (close[index] > uperband[index])):
                    supertrand[index] = lowerband[index]
                else:
                    if((supertrand[index -1] == lowerband[index -1]) & (close[index] > lowerband[index])):
                        supertrand[index] = lowerband[index]
                    else:
                        if((supertrand[index -1] == lowerband[index -1]) & (close[index] < lowerband[index])):
                            supertrand[index] = uperband[index]
                        else:
                            supertrand[index] = ""
    return supertrand

# clo = [
# 8284,
# 8395.45,
# 8378.4,
# 8127.35,
# 8102.1,
# 8234.6,
# 8284.5,
# 8323,
# 8299.4,
# 8277.55,
# 8494.15,
# 8513.8,
# 8550.7,
# 8695.6,
# 8729.5,
# 8761.4,
# 8835.6,
# 8910.5,
# 8914.3,
# 8952.35,
# 8808.9,
# 8797.4,
# 8756.55,
# 8723.7,
# 8711.7,
# 8661.05,
# 8526.35,
# 8565.55,
# 8627.4
# ]
# hi = [
# 8294.7,
# 8410.6,
# 8445.6,
# 8327.85,
# 8151.2,
# 8243.5,
# 8303.3,
# 8332.6,
# 8356.65,
# 8326.45,
# 8527.1,
# 8530.75,
# 8570.95,
# 8707.9,
# 8741.85,
# 8774.15,
# 8866.4,
# 8925.05,
# 8985.05,
# 8966.65,
# 8996.6,
# 8840.8,
# 8837.3,
# 8792.85,
# 8838.45,
# 8726.2,
# 8605.55,
# 8646.25,
# 8651.95
# ]
#
# lo = [
# 8248.75,
# 8288.7,
# 8363.9,
# 8111.35,
# 8065.45,
# 8167.3,
# 8190.8,
# 8245.6,
# 8267.9,
# 8236.65,
# 8380.55,
# 8452.25,
# 8531.5,
# 8574.5,
# 8689.6,
# 8727,
# 8795.4,
# 8825.45,
# 8874.05,
# 8861.25,
# 8775.1,
# 8751.1,
# 8726.65,
# 8704.4,
# 8683.65,
# 8645.55,
# 8516.35,
# 8470.5,
# 8593.65
# ]
#
# df = pd.DataFrame()
# df['H'] = hi
# df['L'] = lo
# df['C'] = clo
#
# print supertrand(df)