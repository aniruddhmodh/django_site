import datetime 	
from django.db import models
from django.utils import timezone

class StocksStrategy(models.Model):
    strategies = models.CharField(max_length = 200)
    scrip = models.CharField(max_length = 200)
    parameters = models.CharField(max_length = 1000, null=True)
    profit = models.FloatField(max_length = 200)
    average = models.FloatField(max_length= 200, null=True)
    winrate = models.FloatField(max_length=200, null=True)
    trade =models.FloatField(max_length=200, null=True)
    default_value = models.BooleanField(default=False)
    pf = models.FloatField(max_length=200, null=True)
    def __str__(self):
        return self.strategies
    