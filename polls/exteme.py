import pandas as pd
import pybacktest as pbt
import ema, wma, dema, tema, supertrand, chande_vidya, trix, macd, rsi
from .models import StocksStrategy
from django.db import connection
from django.db.models import Max

# nifty_data = pbt.load_from_yahoo('^nsei', start = '2014' , adjust_close= True)

def datas(symbols):
    return {symbol: pbt.load_from_yahoo(symbol+'.ns', start='2014') for symbol in symbols}

def crossover(ms, ml):
    buy = (ms > ml) & (ms.shift() < ml.shift())
    return buy

def crossunder(ms, ml):
    sell = (ms < ml) & (ms.shift() > ml.shift())
    return sell

def sma_cross(ohlc, period_small = 50, period_long = 100):
    ml = pd.rolling_mean(ohlc.C, period_long)
    buy = cover = crossover(ms, ml)
    sell = short = crossunder(ms, ml)
    return locals()

def ema_cross(ohlc, period_small = 50, period_long = 100):
    ms = ema.expon_moving_avg(ohlc.C, period_small)
    ml = ema.expon_moving_avg(ohlc.C, period_long)
    buy = cover = crossover(ms, ml)
    sell = short = crossunder(ms, ml)
    return locals()

def wma_ccross(ohlc, period_small = 50, period_long = 100):
    ms = wma.weighted_moving_average(ohlc.C, period_small)
    ml = wma.weighted_moving_average(ohlc.C, period_long)
    buy = cover = crossover(ms, ml)
    sell = short = crossunder(ms, ml)
    return locals()

def dema_cross(ohlc, period_small = 50, period_long = 100):
    ms = dema.double_exponential_moving_average(ohlc.C, period_small)
    ml = dema.double_exponential_moving_average(ohlc.C, period_long)
    buy = cover = crossover(ms, ml)
    sell = short = crossunder(ms, ml)
    return locals()

def tema_cross(ohlc, period_small = 50, period_long = 100):
    ms = tema.expon_moving_avg(ohlc.C, period_small)
    ml = tema.expon_moving_avg(ohlc.C, period_long)
    buy = cover = crossover(ms, ml)
    sell = short = crossunder(ms, ml)
    return locals()

def supertrand_cross(ohlc, period_small = 10, multiplier = 3):
    ml = supertrand.supertrand(ohlc, period_small, multiplier)
    buy = cover = crossover(ohlc.C, ml)
    sell = short = crossunder(ohlc.C, ml)
    return locals()

def chandevidya(ohlc, period = 50):
    ml = chande_vidya.chande_vidya(ohlc.C, period)
    buy = cover = crossover(ohlc.C, ml)
    sell = short = crossunder(ohlc.C, ml)
    return locals()

def trix_cross(ohlc, trix_period = 9, avg_period = 9):
    ms = trix.trix(ohlc.C, trix_period)
    ml = pd.rolling_mean(ms, avg_period)
    buy = cover = crossover(ms, ml)
    sell = short = crossunder(ms, ml)
    return locals()

def macd_cross(ohlc, small_ema = 12, long_ema = 26, avg_period = 9):
    ms = macd.macd(ohlc.C, small_ema, long_ema)
    ml = pd.rolling_mean(ms, avg_period)
    buy = cover = crossover(ms, ml)
    sell = short = crossunder(ms, ml)
    return locals()

def rsi_cross(ohlc, rsi_period = 14, avg_period = 9):
    ms = rsi.rsi(ohlc.C, rsi_period)
    ml = pd.rolling_mean(ms, avg_period)
    buy = cover = crossover(ms, ml)
    sell = short = crossunder(ms, ml)
    return locals()

all_strategies = [supertrand_cross] #[chandevidya,supertrand_cross,trix_cross, macd_cross, rsi_cross]#[sma_cross, ema_cross, wma_ccross, dema_cross, tema_cross]

def sort_by_strategy(strategies = all_strategies):
    final_dict = {}
    nifty_all = pd.read_csv('/home/sigmasoft6/Desktop/indicator/nifty_all.csv')
    data = dict(zip(nifty_all['Symbol'], nifty_all['Company']))
    all_scripts = datas(data.keys()[:2])
    for strategy in strategies:
        final_dict.update({strategy.__name__: [{scrip:pbt.Backtest(strategy(all_scripts[scrip]),strategy.__name__).report['performance']['profit']} for scrip in all_scripts]})
    return final_dict

def sort_by_scripts(strategies = all_strategies):
    final_dict = {}
    nifty_all = pd.read_csv('/home/sigmasoft6/Desktop/indicator/nifty_all.csv')
    data = dict(zip(nifty_all['Symbol'], nifty_all['Company']))
    all_scripts = datas(data.keys()[:2])
    for script in all_scripts:
        final_dict.update({script:[{stgy.__name__: stgy(all_scripts[script]).report['performance']['profit']} for stgy in strategies]})
    return final_dict

def save_strategy(strategies = all_strategies):
    final_dict = {}
    #for insert into database
    # nifty_all = pd.read_csv('/home/sigmasoft6/Desktop/indicator/nifty_all.csv')
    # data = dict(zip(nifty_all['Symbol'], nifty_all['Company']))
    # all_scripts = datas(data.keys())
    # for scrip in all_scripts:
    #     ohlc = all_scripts[scrip]
    #     for strategy in strategies:
    #         strategy_name = strategy.__name__
    #         bt = pbt.Backtest(strategy(ohlc), strategy_name)
    #         if 'performance' in bt.report:
    #             profit = bt.report['performance']['profit']
    #             average = bt.report['performance']['averages']['loss']
    #             winrate = bt.report['performance']['winrate']
    #             trades = bt.report['exposure']['trades/month']
    #             pf = bt.report['performance']['PF']
    #             StocksStrategy(strategies = strategy.__name__, scrip = scrip, parameters ={'period_small':10,'avg_period':3} , profit = profit, average = average, winrate = winrate, trade = trades, default_value = True, pf = pf).save()
    #==============================
    #For dict type data display
    # cursor = connection.cursor()
    # cursor.execute("select distinct strategies from polls_StocksStrategy where default_value = true")
    # strategies = cursor.fetchall()
    # for strategy in strategies:
    #     # cursor.execute("select script, profit from polls_stocks_per_strategies where strategies='%s' order by profit DESC"%(strategy))
    #     cursor.execute("select scrip, profit from polls_StocksStrategy where strategies='%s' and default_value = true order by profit DESC"%(strategy))
    #     scripts = cursor.fetchall()
    #     final_dict.update({str(strategy[0]): [{scripts[index][0]:scripts[index][1]} for index in range(len(scripts))]})
    return final_dict
    #===========================

def save_script(strategies = all_strategies):
    final_dict = {}
    # cursor = connection.cursor()
    # cursor.execute("select distinct scrip from polls_StocksStrategy where default_value = true")
    # scripts = cursor.fetchall()
    # for script in scripts:
    #     # cursor.execute("select strategies, profit from polls_stocks_per_strategies where script='%s' order by profit DESC"%(script))
    #     cursor.execute("select strategies, profit from polls_StocksStrategy where scrip='%s' and default_value = true order by profit DESC"%(script))
    #     strategies = cursor.fetchall()
    #     final_dict.update({str(script[0]) : [{strategies[index][0]:strategies[index][1]} for index in range(len(strategies))]})
    return final_dict

all_strategies_new = [chandevidya]#[sma_cross, ema_cross, wma_ccross, dema_cross, tema_cross]

def optimized_strategies(strategies = all_strategies_new):
    final_dict = {}
    nifty_all = pd.read_csv('/home/sigmasoft6/Desktop/indicator/nifty_all.csv')
    data = dict(zip(nifty_all['Symbol'], nifty_all['Company']))
    all_scripts = datas(data.keys())
    for strategy in strategies:
        for script in all_scripts:
                ohlc = all_scripts[script]
                optimizer = pbt.Optimizer(strategy, ohlc, {'period_small': (50, 55, 1), 'period_long': (100, 102, 1)}, metrics = ['profit','average', 'winrate', 'trades','pf'], processes=1)
                # optimizer = pbt.Optimizer(strategy, ohlc, {'small_ema': (12, 14, 1), 'long_ema': (24, 25, 1), 'avg_period': (9, 10, 1)}, metrics = ['profit','average', 'winrate', 'trades','pf'], processes=1)
                # optimizer = pbt.Optimizer(strategy, ohlc, {'trix_period': (5, 7, 1), 'avg_period':(10,12,1)}, metrics = ['profit','average', 'winrate', 'trades'], processes=1)
                for index in range(len(optimizer.results)):
                    period_small = optimizer.results['period_small'][index]
                    period_long = optimizer.results['period_long'][index]
                    # avg_period = optimizer.results['avg_period'][index]
                    profit = optimizer.results['profit'][index]
                    average = optimizer.results['average'][index]
                    winrate = optimizer.results['winrate'][index]
                    trades = optimizer.results['trades'][index]
                    pf = optimizer.results['pf'][index]
                    StocksStrategy(strategies = strategy.__name__, scrip = script, parameters = '{period_small: %s, period_long : %s}'%(period_small, period_long), profit = profit, average = average, winrate= winrate, trade= trades,pf=pf).save()
                    final_dict.update({strategy:{script:[period_small,period_long, profit]}})
    return final_dict

def opt_from_tbl():
    cursor = connection.cursor()
    cursor.execute("Select DISTINCT scrip as s, strategies, profit, parameters FROM polls_StocksStrategy where default_value = 'False' GROUP BY s ORDER BY profit DESC")
    data_all =cursor.fetchall()
    return data_all
