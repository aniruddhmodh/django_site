from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^polls/', include('polls.urls')),
    url(r'^polls1/', include('polls1.urls')),
    url(r'^admin/', admin.site.urls),
]
